import SoapHandler from './src/SoapHandler'
import argsMaker from './src/argsMaker'
import {SoapSmsEnvData,SmsRequestParams,SmsRequestResponse} from './src/Models';


class SopranoSmsCommunicator {
    private SoapHandler:SoapHandler;
    private SoapSmsEnvData:SoapSmsEnvData;
    constructor(SoapSmsEnvData: SoapSmsEnvData) {
        this.SoapHandler= new SoapHandler(SoapSmsEnvData);
        this.SoapSmsEnvData= SoapSmsEnvData;
    }

    public async SendText(SmsRequestParams:SmsRequestParams){
        let argsCreator : argsMaker = new argsMaker(this.SoapSmsEnvData);
        const args : SmsRequestResponse = argsCreator.createArgs(SmsRequestParams);
        try{
            const result = await this.SoapHandler.sendText(args);
            return result;
        }
        catch(err){
            throw err;
        }
    }
    
}
//ts has issue with es6 export write it as module.exports instead
module.exports=SopranoSmsCommunicator;