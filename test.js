const SopranoSmsCommunicator = require('./lib/index.js')
const conf = require('./conf.js');
const Soprano = new SopranoSmsCommunicator(
    conf.SoapSmsEnvData
);
Soprano.SendText(conf.SmsRequestParams)
    .then(res => {
        console.log(res);
    }).catch(err => {
        console.error(err);
    })
