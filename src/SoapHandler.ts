import { createClient } from "soap";
import { SmsRequestParams, SoapSmsEnvData } from "./Models";
export default class SoapHandler {
  private wsdl_url: string;
  constructor(SoapSmsEnvData: SoapSmsEnvData) {
    this.wsdl_url = SoapSmsEnvData.url;
  }
  sendText(args: SmsRequestParams): Promise<any> {
    return new Promise((resolve, reject) => {
      createClient(this.wsdl_url, (err, client) => {
        if (client) {
          client.SendTextSingle(args, (err: any, res: any) => {
            if (res) {
                if(res.SendTextSingleResult.SendSmsResponse.isPositive=="true"){
                    resolve(res.SendTextSingleResult.SendSmsResponse);
                }else{
                   reject(res.SendTextSingleResult.SendSmsResponse.descriptions.string)
                }
            } else {
              reject(err);
            }
          });
        } else reject(err);
      });
    });
  }
}
