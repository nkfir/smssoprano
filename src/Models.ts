

export interface SoapSmsEnvData{
    user : string;
    password : string; 
    sender: string;
    url : string;
}
export interface SmsRequestParams{
    destination:string;
    payload:string;
    reference:string;
}
export interface SmsRequestResponse{
    userId:string;
    password: string;
    originator:string;
    destination:string;
    payload:string;
    vp: number;
    reference:string;
    delayDeliveryMin:number;
    isTest:boolean
}