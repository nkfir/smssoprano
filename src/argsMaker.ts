import { SoapSmsEnvData ,SmsRequestParams,SmsRequestResponse} from './Models';

export default class argsMaker{
    private SoapSmsEnvData : SoapSmsEnvData;
    constructor(
            SoapSmsEnvData : SoapSmsEnvData,
        ){    
        this.SoapSmsEnvData = SoapSmsEnvData;
    }
    public createArgs(SmsRequestParams:SmsRequestParams):SmsRequestResponse{
        return{
            userId:this.SoapSmsEnvData.user,
            password: this.SoapSmsEnvData.password,
            originator:this.SoapSmsEnvData.sender,
            destination:SmsRequestParams.destination,
            payload:SmsRequestParams.payload,
            vp: 86400,
            reference:SmsRequestParams.reference,
            delayDeliveryMin:0,
            isTest:false
        }
    }
}