import { SoapSmsEnvData, SmsRequestParams, SmsRequestResponse } from './Models';
export default class argsMaker {
    private SoapSmsEnvData;
    constructor(SoapSmsEnvData: SoapSmsEnvData);
    createArgs(SmsRequestParams: SmsRequestParams): SmsRequestResponse;
}
