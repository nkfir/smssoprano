"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var soap_1 = require("soap");
var SoapHandler = /** @class */ (function () {
    function SoapHandler(SoapSmsEnvData) {
        this.wsdl_url = SoapSmsEnvData.url;
    }
    SoapHandler.prototype.sendText = function (args) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            soap_1.createClient(_this.wsdl_url, function (err, client) {
                if (client) {
                    client.SendTextSingle(args, function (err, res) {
                        if (res) {
                            if (res.SendTextSingleResult.SendSmsResponse.isPositive == "true") {
                                resolve(res.SendTextSingleResult.SendSmsResponse);
                            }
                            else {
                                reject(res.SendTextSingleResult.SendSmsResponse.descriptions.string);
                            }
                        }
                        else {
                            reject(err);
                        }
                    });
                }
                else
                    reject(err);
            });
        });
    };
    return SoapHandler;
}());
exports.default = SoapHandler;
//# sourceMappingURL=SoapHandler.js.map