import { SmsRequestParams, SoapSmsEnvData } from "./Models";
export default class SoapHandler {
    private wsdl_url;
    constructor(SoapSmsEnvData: SoapSmsEnvData);
    sendText(args: SmsRequestParams): Promise<any>;
}
