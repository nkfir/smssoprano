"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var argsMaker = /** @class */ (function () {
    function argsMaker(SoapSmsEnvData) {
        this.SoapSmsEnvData = SoapSmsEnvData;
    }
    argsMaker.prototype.createArgs = function (SmsRequestParams) {
        return {
            userId: this.SoapSmsEnvData.user,
            password: this.SoapSmsEnvData.password,
            originator: this.SoapSmsEnvData.sender,
            destination: SmsRequestParams.destination,
            payload: SmsRequestParams.payload,
            vp: 86400,
            reference: SmsRequestParams.reference,
            delayDeliveryMin: 0,
            isTest: false
        };
    };
    return argsMaker;
}());
exports.default = argsMaker;
//# sourceMappingURL=argsMaker.js.map